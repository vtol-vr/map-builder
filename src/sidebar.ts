import { Settings } from "./types";

export default class Sidebar {
  nameInput: HTMLInputElement;
  chunksInput: HTMLInputElement;
  biomeInput: HTMLInputElement;
  edgeModeInput: HTMLInputElement;
  coastSideInput: HTMLInputElement;
  seaLevelInput: HTMLInputElement;
  forceLandInput: HTMLInputElement;
  forceLandLevelInput: HTMLInputElement;
  forceGrassInput: HTMLInputElement;
  forceGrassLevelInput: HTMLInputElement;
  forceWaterInput: HTMLInputElement;
  forceWaterLevelInput: HTMLInputElement;
  simpleWaterInput: HTMLInputElement;
  scaleInput: HTMLInputElement;
  apiKeyInput: HTMLInputElement;
  generateButton: HTMLButtonElement;
  constructor(settings: Settings, settingsCallback: (settings: Settings) => any, buttonCallback: () => any) {
    this.nameInput = document.getElementById('name') as HTMLInputElement;
    this.nameInput.value = settings.name;
    this.nameInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.chunksInput = document.getElementById('chunks') as HTMLInputElement;
    this.chunksInput.value = settings.chunks.toString();
    this.chunksInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.biomeInput = document.getElementById('biome') as HTMLInputElement;
    this.biomeInput.value = settings.biome;
    this.biomeInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.edgeModeInput = document.getElementById('edgeMode') as HTMLInputElement;
    this.edgeModeInput.value = settings.edgeMode;
    this.edgeModeInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.coastSideInput = document.getElementById('coastSide') as HTMLInputElement;
    this.coastSideInput.value = settings.coastSide;
    this.coastSideInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.seaLevelInput = document.getElementById('seaLevel') as HTMLInputElement;
    this.seaLevelInput.value = settings.seaLevel.toString();
    this.seaLevelInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.forceLandInput = document.getElementById('forceLand') as HTMLInputElement;
    this.forceLandInput.checked = settings.forceLand;
    this.forceLandInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.forceLandLevelInput = document.getElementById('forceLandLevel') as HTMLInputElement;
    this.forceLandLevelInput.value = settings.forceLandLevel.toString();
    this.forceLandLevelInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.forceGrassInput = document.getElementById('forceGrass') as HTMLInputElement;
    this.forceGrassInput.checked = settings.forceGrass;
    this.forceGrassInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.forceGrassLevelInput = document.getElementById('forceGrassLevel') as HTMLInputElement;
    this.forceGrassLevelInput.value = settings.forceGrassLevel.toString();
    this.forceGrassLevelInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.forceWaterInput = document.getElementById('forceWater') as HTMLInputElement;
    this.forceWaterInput.checked = settings.forceWater;
    this.forceWaterInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.forceWaterLevelInput = document.getElementById('forceWaterLevel') as HTMLInputElement;
    this.forceWaterLevelInput.value = settings.forceWaterLevel.toString();
    this.forceWaterLevelInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.simpleWaterInput = document.getElementById('simpleWater') as HTMLInputElement;
    this.simpleWaterInput.checked = settings.simpleWater;
    this.simpleWaterInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.scaleInput = document.getElementById('scale') as HTMLInputElement;
    this.scaleInput.value = settings.scale.toString();
    this.scaleInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.apiKeyInput = document.getElementById('apiKey') as HTMLInputElement;
    this.apiKeyInput.value = settings.apiKey;
    this.apiKeyInput.onchange = () => {
      settingsCallback(this.getSettings());
    }
    this.generateButton = document.getElementById('generate') as HTMLButtonElement;
    this.generateButton.onclick = () => {
      buttonCallback();
    }
  }
  getSettings(): Settings {
    return {
      name: this.nameInput.value,
      chunks: parseInt(this.chunksInput.value),
      biome: this.biomeInput.value,
      edgeMode: this.edgeModeInput.value,
      coastSide: this.coastSideInput.value,
      seaLevel: parseFloat(this.seaLevelInput.value),
      forceLand: this.forceLandInput.checked,
      forceLandLevel: parseFloat(this.forceLandLevelInput.value),
      forceGrass: this.forceGrassInput.checked,
      forceGrassLevel: parseFloat(this.forceGrassLevelInput.value),
      forceWater: this.forceWaterInput.checked,
      forceWaterLevel: parseFloat(this.forceWaterLevelInput.value),
      simpleWater: this.simpleWaterInput.checked,
      scale: parseFloat(this.scaleInput.value),
      apiKey: this.apiKeyInput.value
    };
  }
}