import axios from "axios";
import { TerrainTile, VectorTile } from "./types";
import { PNG } from 'pngjs/browser';
import * as topojson from 'topojson-client';

export default class TileCache {
  terrainTiles: TerrainTile[] = [];
  vectorTiles: VectorTile[] = [];
  async getTerrainTile(zoom: number, x: number, y: number, apiKey: string): Promise<TerrainTile> {
    if(zoom < 0) {
      throw 'No tile available!';
    }
    for(let terrainTile of this.terrainTiles) {
      if(terrainTile.zoom === zoom && terrainTile.x === x && terrainTile.y === y) {
        return terrainTile;
      }
    }
    try {
      let png = await axios.get(`https://tile.nextzen.org/tilezen/terrain/v1/256/terrarium/${zoom}/${x}/${y}.png?api_key=${apiKey}`, { responseType: 'arraybuffer' });
      let newTerrainTile: TerrainTile = {
        zoom: zoom,
        x: x,
        y: y,
        png: PNG.sync.read(Buffer.from(png.data))
      };
      this.terrainTiles.push(newTerrainTile);
      return newTerrainTile;
    }
    catch(err) {
      let parentTerrainTile = await this.getTerrainTile(zoom - 1, Math.floor(x/2), Math.floor(y/2), apiKey);
      let png = new PNG({
        width: 256,
        height: 256,
        inputHasAlpha: true
      });
      let xOffset = (x % 2) * 128;
      let yOffset = (y % 2) * 128;
      for(let y = 0; y < png.height;y++) {
        for(let x = 0; x < png.width;x++) {
          let idx = (png.width * y + x) << 2;
          let idx2 = (png.width * (Math.floor(y / 2) + yOffset) + (Math.floor(x / 2) + xOffset)) << 2;
          png.data[idx] = parentTerrainTile.png.data[idx2];
          png.data[idx+1] = parentTerrainTile.png.data[idx2+1];
          png.data[idx+2] = parentTerrainTile.png.data[idx2+2];
          png.data[idx+3] = parentTerrainTile.png.data[idx2+3];
        }
      }
      let newTerrainTile: TerrainTile = {
        zoom: zoom,
        x: x,
        y: y,
        png: png
      };
      this.terrainTiles.push(newTerrainTile);
      return newTerrainTile;
    }
  }
  async getVectorTile(zoom: number, x: number, y: number, apiKey: string): Promise<VectorTile> {
    if(zoom < 0) {
      throw 'No tile available!';
    }
    for(let vectorTile of this.vectorTiles) {
      if(vectorTile.zoom === zoom && vectorTile.x === x && vectorTile.y === y) {
        return vectorTile;
      }
    }
    try {
      let vector = await axios.get(`https://tile.nextzen.org/tilezen/vector/v1/256/all/${zoom}/${x}/${y}.topojson?api_key=${apiKey}`, { responseType: 'json' });
      let featureCollection: any = [];
      featureCollection.push(topojson.feature(vector.data, 'water').features);
      featureCollection.push(topojson.feature(vector.data, 'roads').features);
      featureCollection.push(topojson.feature(vector.data, 'buildings').features);
      featureCollection.push(topojson.feature(vector.data, 'earth').features);
      let featureCollectionObject = {
        type: 'FeatureCollection',
        features: featureCollection.flat()
      };
      let newVectorTile: VectorTile = {
        zoom: zoom,
        x: x,
        y: y,
        features: featureCollectionObject
      };
      this.vectorTiles.push(newVectorTile);
      return newVectorTile;
    }
    catch(err) {
      let parentTile = await this.getVectorTile(zoom - 1, Math.floor(x/2), Math.floor(y/2), apiKey);
      let newVectorTile: VectorTile = {
        zoom: zoom,
        x: x,
        y: y,
        features: parentTile.features
      };
      this.vectorTiles.push(newVectorTile);
      return newVectorTile;
    }
  }
}