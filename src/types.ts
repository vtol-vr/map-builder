export interface Settings {
  name: string;
  chunks: number;
  biome: string;
  edgeMode: string;
  coastSide: string;
  seaLevel: number;
  forceLand: boolean;
  forceLandLevel: number;
  forceGrass: boolean;
  forceGrassLevel: number;
  forceWater: boolean;
  forceWaterLevel: number;
  simpleWater: boolean;
  scale: number;
  apiKey: string;
}

export type HeightMap = number[][];

export interface Bounds {
  latNorth: number;
  lngEast: number;
  latSouth: number;
  lngWest: number;
}

export interface TerrainTile {
  zoom: number;
  x: number;
  y: number;
  png: any;
}

export interface VectorTile {
  zoom: number;
  x: number;
  y: number;
  features: any;
}

export interface VTMEntities {
  staticPrefabs: StaticPrefab[];
}

export interface StaticPrefab {
  prefab: string;
  id: number;
  position: Position;
  rotation: Rotation
}

export interface Position {
  x: number;
  y: number;
  z: number;
}

export interface Rotation {
  x: number;
  y: number;
  z: number;
}