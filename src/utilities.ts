export function chunksToMeter(chunks: number): number {
  return chunks * 3.072 * 1000;
}

export function zoomLevel(lat: number, minPixelResolution: number, maxZoom: number = 20): number {
  let targetZoom = maxZoom;
  let latRad = lat * (Math.PI / 180);
  for(let z = targetZoom;z >= 0;z--) {
    let distancePerPixel = 40075016.686 * (Math.cos(latRad) / Math.pow(2,z + 8));
    if(distancePerPixel <= minPixelResolution) {
      targetZoom = z;
    }
  }
  return targetZoom;
}

export function slippyX(lng: number, zoom: number): number {
  let n = Math.pow(2, zoom);
  return (n * ((lng + 180)/360));
}

export function slippyY(lat: number, zoom: number): number {
  let n = Math.pow(2, zoom);
  let latRad = lat * (Math.PI / 180);
  return (n * (1 - (Math.log(Math.tan(latRad) + (1 / Math.cos(latRad))) / Math.PI)) / 2);
}

export function nameToId(name: string): string {
  return name.replace(/ /g,'_').toLowerCase();
}

export function mapX(x: number, chunks: number, edgeMode: string, coastSide: string): number {
  return (x) * 153.6;
}

export function mapY(y: number, chunks: number, edgeMode: string, coastSide: string): number {
  let mapWidth = chunks * 20 + 1;
  return ((mapWidth - y) - 1) * 153.6;
}