import { Settings, StaticPrefab } from "./types";
import MapGenerator from "./generator";
import { mkdirSync, rmSync, writeFileSync } from "fs";
import { mapX, mapY, nameToId } from "./utilities";

const generateTestMap = async (chunks: number, edgeMode: string, coastSide: string, checkerBoardWidth: number, checkerBoardMaxHeight: number, checkerBoardMinHeight: number) => {
  let mapWidth = chunks * 20 + 1;
  let mapGenerator = new MapGenerator();
  let staticPrefabs: StaticPrefab[] = [];
  for(let x = 0;x < mapWidth;x++) {
    mapGenerator.data[x] = [];
    for(let y = 0;y < mapWidth;y++) {
      if(x % (checkerBoardWidth * 2) < checkerBoardWidth && y % (checkerBoardWidth * 2) < checkerBoardWidth) {
        if(x >= checkerBoardWidth * 7 || y >= checkerBoardWidth * 5) {
          mapGenerator.data[x][y] = checkerBoardMaxHeight;
        }
        else {
          mapGenerator.data[x][y] = checkerBoardMinHeight;
        }
      }
      else {
        mapGenerator.data[x][y] = checkerBoardMinHeight;
      }
      if(x % (checkerBoardWidth * 2) === 0 && y % (checkerBoardWidth * 2) === 0) {
        staticPrefabs.push({
          prefab: 'JpnTwrHPad',
          id: staticPrefabs.length,
          position: {
            x: mapX(x, chunks, edgeMode, coastSide),
            y: mapY(y, chunks, edgeMode, coastSide),
            z: checkerBoardMaxHeight
          },
          rotation: { x: 0, y: 0, z: 0 }
        });
      }
    }
  }
  let settings: Settings = {
    name: `Test Checkerboard ${edgeMode} ${coastSide} ${chunks} ${checkerBoardWidth} ${checkerBoardMinHeight} ${checkerBoardMaxHeight}`,
    chunks: chunks,
    biome: 'Boreal',
    edgeMode: edgeMode,
    coastSide: coastSide,
    seaLevel: 0,
    forceLand: false,
    forceLandLevel: 0,
    forceGrass: false,
    forceGrassLevel: 0,
    forceWater: false,
    forceWaterLevel: 0,
    simpleWater: false,
    scale: 1,
    apiKey: 'NaqqS33fTUmyQcvbuIUCKA'
  };
  let zipFile = await mapGenerator.zipFiles(settings, { staticPrefabs: staticPrefabs });
  writeFileSync('test-maps/' + nameToId(settings.name) + '.zip', Buffer.from(await zipFile.arrayBuffer()));
}

try { rmSync('test-maps', { force: true, recursive: true }); } catch(err) {}
try { mkdirSync('test-maps'); } catch(err) {}
(async () => {
  await generateTestMap(8, 'Water', 'North', 10, 1000, 10);
  await generateTestMap(8, 'Water', 'North', 20, 1000, 10);
  await generateTestMap(16, 'Water', 'North', 10, 1000, 10);
  await generateTestMap(16, 'Water', 'North', 40, 1000, 10);
  await generateTestMap(16, 'Hills', 'North', 10, 1000, 10);
})();