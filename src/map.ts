import leaflet from 'leaflet';
import Tangram from 'tangram';
import { Bounds, Settings } from './types';
import { chunksToMeter } from './utilities';
import haversineOffset from 'haversine-offset';

export default class MapManager {
  map: any;
  rectangle: any;
  settings: Settings;
  scene: Tangram;
  constructor(elementId: string, settings: Settings) {
    this.map = leaflet.map(elementId, {
      scrollWheelZoom: 'center'
    }).setView([51.505, -0.09], 11);
    let sceneUrl = new URL('scene.yaml', import.meta.url);
    let layer = Tangram.leafletLayer({
      scene: sceneUrl.toString(),
      attribution: '<a href="https://mapzen.com/tangram" target="_blank">Tangram</a> | &copy; OSM contributors'
    }).addTo(this.map);
    this.scene = layer.scene;
    leaflet.control.scale().addTo(this.map);
    this.settings = settings;
    this.rectangle = leaflet.rectangle(this.getLeafletBounds(), { color: '#ff0000', weight: 1 });
    this.rectangle.addTo(this.map);
    this.map.on('zoom', () => {
      this.update();
    });
    this.map.on('move', () => {
      this.update();
    });
    this.updateSettings(settings);
  }
  update() {
    this.rectangle.setBounds(this.getLeafletBounds());
  }
  updateSettings(settings: Settings) {
    this.settings = settings;
    if(this.scene.config !== null) {
      this.scene.config.sources.elevation.url_params.api_key = settings.apiKey;
      this.scene.config.sources.mapzen.url_params.api_key = settings.apiKey;
      this.scene.config.styles.hillshade.shaders.uniforms.u_waterlevel = settings.seaLevel;
      this.scene.updateConfig();
    }
    this.update();
  }
  getLeafletBounds() {
    let side = chunksToMeter(this.settings.chunks) * this.settings.scale;
    let center = {
      latitude: this.map.getCenter().lat,
      longitude: this.map.getCenter().lng
    };
    let corner1 = haversineOffset(center, { x: side/2, y: side/2 });
    let corner2 = haversineOffset(center, { x: -side/2, y: -side/2 });
    return new leaflet.LatLngBounds(new leaflet.LatLng(corner1.lat,corner1.lng),new leaflet.LatLng(corner2.lat,corner2.lng));
  }
  getBounds(): Bounds {
    let leafletBounds = this.getLeafletBounds();
    let newBounds: Bounds = {
      latNorth: leafletBounds.getNorth(),
      lngEast: leafletBounds.getEast(),
      latSouth: leafletBounds.getSouth(),
      lngWest: leafletBounds.getWest()
    };
    return newBounds;
  }
}