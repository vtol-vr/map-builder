import TileCache from "./cache";
import { Bounds, HeightMap, Settings, VTMEntities } from "./types";
import { PNG } from 'pngjs/browser';
import * as jpeg from 'jpeg-js';
import download from 'downloadjs';
import { nameToId, slippyX, slippyY, zoomLevel } from "./utilities";
import MapTemplate from "./template";
import whichPolygon from 'which-polygon';
const { BlobReader, BlobWriter, TextReader, ZipWriter } = require("@zip.js/zip.js");

const LANDKINDS = ['earth'];
const WATERKINDS = ['water','ocean','lake'];
const VTOLVRSEALEVEL = -5;

export default class MapGenerator {
  data: HeightMap = [];
  tileCache: TileCache = new TileCache();
  templates: MapTemplate = new MapTemplate();
  generateButton: HTMLButtonElement;
  feedbackText: HTMLElement;
  constructor() {
    if(typeof document !== 'undefined') {
      this.generateButton = document.getElementById('generate') as HTMLButtonElement;
      this.feedbackText = document.getElementById('feedback') as HTMLElement;
    }
  }
  async generate(settings: Settings, bounds: Bounds) {
    //Disable Generate Button
    this.generateButton.disabled = true;
    //Get the dimensions of the VTOL VR map image
    let mapWidth = settings.chunks * 20 + 1;
    //Create grid of lat / lng coordinates to match VTOL VR map dimensions
    let latHeight = Math.abs(bounds.latNorth - bounds.latSouth);
    let lngWidth = Math.abs(bounds.lngEast - bounds.lngWest);
    for(let x = 0;x < mapWidth;x++) {
      this.data[x] = [];
      for(let y = 0;y < mapWidth;y++) {
        //Show progress
        this.feedbackText.innerText = `Generating height map: ${y + (x * mapWidth)} / ${mapWidth * mapWidth} (${((y + (x * mapWidth)) * 100 / (mapWidth * mapWidth)).toFixed(1)}%)`;
        let lat = bounds.latNorth - (latHeight * (y / (mapWidth - 1)));
        let lng = bounds.lngWest + (lngWidth * (x / (mapWidth - 1)));
        //Get elevation from terrain data
        let elevation = await this.getElevation(lat, lng, settings.apiKey);
        this.data[x][y] = elevation - settings.seaLevel;
        //Get terrain kind from vector data if needed
        if(settings.forceLand || settings.forceWater) {
          let kinds = await this.getKinds(lat, lng, settings.apiKey);
          let forced = false;
          for(let kind of kinds) {
            if(settings.forceLand && this.data[x][y] <= VTOLVRSEALEVEL && !forced && LANDKINDS.includes(kind)) {
              this.data[x][y] = settings.forceLandLevel;
              forced = true;
            }
            if(settings.forceWater && this.data[x][y] >= VTOLVRSEALEVEL && !forced && WATERKINDS.includes(kind)) {
              this.data[x][y] = settings.forceWaterLevel;
              forced = true;
            }
          }
        }
        //Force terrain to grass height for land below grass height
        if(settings.forceGrass && this.data[x][y] >= VTOLVRSEALEVEL) {
          this.data[x][y] = Math.max(this.data[x][y], settings.forceGrassLevel);
        }
        //Force terrain to water height to simplify water
        if(settings.simpleWater && this.data[x][y] < VTOLVRSEALEVEL) {
          this.data[x][y] = settings.forceWaterLevel;
        }
      }
    }
    this.feedbackText.innerText = `Generating final files`;
    download(await this.zipFiles(settings, { staticPrefabs: [] }), nameToId(settings.name) + '.zip', 'application/zip');
    //Enable generate button
    this.generateButton.disabled = false;
    this.feedbackText.innerText = 'Ready to generate!';
  }
  async zipFiles(settings: Settings, entities: VTMEntities) {
    let zipFile = new BlobWriter();
    let zipWriter = new ZipWriter(zipFile);
    await zipWriter.add(`${nameToId(settings.name)}/height0.png`, new BlobReader(new Blob([this.generateHeightMapPng(this.data.length,-80,1440)])));
    await zipWriter.add(`${nameToId(settings.name)}/height1.png`, new BlobReader(new Blob([this.generateHeightMapPng(this.data.length,1440,2960)])));
    await zipWriter.add(`${nameToId(settings.name)}/height2.png`, new BlobReader(new Blob([this.generateHeightMapPng(this.data.length,2960,4480)])));
    await zipWriter.add(`${nameToId(settings.name)}/height3.png`, new BlobReader(new Blob([this.generateHeightMapPng(this.data.length,4480,6000)])));
    await zipWriter.add(`${nameToId(settings.name)}/preview.jpg`, new BlobReader(new Blob([this.generatePreviewJpg(this.data.length)])));
    await zipWriter.add(`${nameToId(settings.name)}/${nameToId(settings.name)}.vtm`, new TextReader(await this.templates.generate(settings, entities)));
    await zipWriter.close();
    return await zipFile.getData();
  }
  async getElevation(lat: number, lng: number, apiKey: string): Promise<number> {
    //Minimum resolution of 150
    let targetZoom = zoomLevel(lat, 150);
    let targetX = slippyX(lng, targetZoom);
    let targetY = slippyY(lat, targetZoom);
    let terrainTile = await this.tileCache.getTerrainTile(targetZoom, Math.floor(targetX), Math.floor(targetY), apiKey);
    let targetPCoords = [
      Math.floor((targetX - Math.floor(targetX)) * 256),
      Math.floor((targetY - Math.floor(targetY)) * 256)
    ];
    let idx = (terrainTile.png.width * targetPCoords[1] + targetPCoords[0]) << 2;
    let rgb = terrainTile.png.data.slice(idx,idx+4);
    return ((rgb[0] * 256) + rgb[1] + (rgb[2] / 256)) - 32768;
  }
  async getKinds(lat: number, lng: number, apiKey: string): Promise<string[]> {
    //Minimum resolution of 50
    let targetZoom = zoomLevel(lat, 50);
    let targetY = slippyY(lat, targetZoom);
    let targetX = slippyX(lng, targetZoom);
    let vectorTile = await this.tileCache.getVectorTile(targetZoom, Math.floor(targetX), Math.floor(targetY), apiKey);
    let query = whichPolygon(vectorTile.features);
    let features = query([lng, lat], true);
    if(features === null) {
      return [];
    }
    let featureKinds = features.sort((a,b) => a?.area - b?.area).map(e => e?.kind);
    return featureKinds;
  }
  generateHeightMapPng(width: number, minElevation: number, maxElevation: number): Buffer {
    let png = new PNG({
      width: width,
      height: width,
      inputHasAlpha: true
    });
    for(let y = 0; y < png.height;y++) {
      for(let x = 0; x < png.width;x++) {
        let idx = (png.width * y + x) << 2;
        let elevation = this.data[x][y];
        let elevationR = (elevation - minElevation) * (256 / (maxElevation - minElevation));
        elevationR = Math.min(255,Math.max(0,elevationR));
        png.data[idx] = Math.floor(elevationR);
        png.data[idx+1] = 0;
        png.data[idx+2] = 0;
        png.data[idx+3] = 255;
      }
    }
    return PNG.sync.write(png);
  }
  generatePreviewJpg(width: number): Buffer {
    let jpg = {
      width: 512,
      height: 512,
      data: new Buffer(512 * 512 * 4)
    };
    for(let y = 0; y < jpg.height;y++) {
      for(let x = 0; x < jpg.width;x++) {
        let idx = (jpg.width * y + x) << 2;
        let elevation = this.data[Math.floor((width - 1) * (x / jpg.width))][Math.floor((width - 1) * (y / jpg.height))];
        if(elevation < VTOLVRSEALEVEL) {
          let waterValue = -((elevation + 80)/80);
          jpg.data[idx] = 0;
          jpg.data[idx+1] = 0;
          jpg.data[idx+2] = Math.floor((waterValue * 0.5 + 0.25) * 255);
        }
        else {
          let landValue = elevation/6000;
          jpg.data[idx] = 0;
          jpg.data[idx+1] = Math.floor((landValue * 0.25 + 0.25) * 255);
          jpg.data[idx+2] = 0;

        }
        jpg.data[idx+3] = 255;
      }
    }
    return jpeg.encode(jpg, 100).data;
  }
}