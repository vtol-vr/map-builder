import MapGenerator from "./generator";
import MapManager from "./map";
import Sidebar from "./sidebar";
import { Settings } from "./types";

export default class App {
  settings: Settings;
  map: MapManager;
  generator: MapGenerator;
  sidebar: Sidebar;
  constructor() {
    this.settings = {
      name: 'Untitled Map',
      chunks: 1,
      biome: 'Boreal',
      edgeMode: 'Water',
      coastSide: 'North',
      seaLevel: 5,
      forceLand: true,
      forceLandLevel: 0,
      forceGrass: false,
      forceGrassLevel: 40,
      forceWater: true,
      forceWaterLevel: -7,
      simpleWater: false,
      scale: 1,
      apiKey: 'NaqqS33fTUmyQcvbuIUCKA'
    };
    this.map = new MapManager('map', this.settings);
    this.generator = new MapGenerator();
    this.sidebar = new Sidebar(this.settings, (s) => {
      this.settings = s;
      this.map.updateSettings(this.settings);
    }, () => {
      this.generator.generate(this.settings, this.map.getBounds());
    });
  }
}